
typedef struct Noeud {
    int cle;
    int occ;
    struct Noeud *gauche;
    struct Noeud *droite;
} T_Noeud;

typedef T_Noeud *T_Arbre;

T_Arbre initABR();

T_Noeud *creerNoeud(int cle);

void ajouterElement(T_Arbre abr, int elem);
