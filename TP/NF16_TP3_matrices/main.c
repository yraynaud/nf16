//************************************************************
// NF16 - TP3 : Les listes chainees
// RAYNAUD Yvain & WU Hanlin
// Exemple d'execution et interface
//************************************************************

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tp3.h"

#define NMAX 100

int main()
{
    printf("\t\t***************************************\n");
    printf("\t\t**     NF16 - TP3 : linked list      **\n");
    printf("\t\t***************************************\n");
    printf("\t\t**   Avril 2020 - RAYNAUD & HANLIN   **\n");
    printf("\t\t***************************************\n\n");
    int nb_m=0;
    struct matrice_creuse tab_m[NMAX];
    int a=1;
    int i,j,val,choix,c,v,ligne,col,reste;
    int k=0;

    do
    {
        printf("Que voulez vous faire ? \n");
        printf("1. Remplir une matrice creuse\n");
        printf("2. Afficher une matrice creuse\n");
        printf("3. Obtenir la valeur d'un element d'une matrice creuse\n");
        printf("4. Changer une valeur un element d'une matrice creuse\n");
        printf("5. Additionner deux matrices creuses\n");
        printf("6. Calculer le gain d'espace en utilisant cette reprsentation pour une matrice donnee\n");
        printf("7. Quitter\n");
        printf("Entrez votre choix:\n");
        scanf("%d",&choix);
        switch(choix)
        {
        case 1: //remplir une matrice creuse
            printf("Attention: Vous pouvez entrer totalement %d matrices\n", NMAX);
            reste=NMAX-nb_m;
            printf("\nIl vous en reste %d.\n",reste);
            printf("\nPour la matrice numero %d, entrez son nombre de ligne(s):\n",nb_m+1);
            scanf("%d",&ligne);
            printf("entrez son nombre de colonne(s):\n");
            scanf("%d",&col);
            if( ligne<=0 || col<=0)
            {
                printf("\Erreur, les valeurs rentr�es sont incoh�rentes avec la matrice choisie\n!");
                break;
            }
            remplirMat(&tab_m[nb_m],ligne,col);
            nb_m++;
            printf("La matrice est bien saisie\n");
            break;
        case 2: //afficher une matrice creuse
            printf("\nQuelle matrice souhaitez vous afficher ? (entrez que le numero)\n");
            printf("Voici les matrices enregistrees :\t");
            while(k<nb_m)
            {
                printf("m%d\t",k+1);
                k++;
            }
            k=0;
            printf("\nVotre choix : ");
            scanf("%d",&c);
            printf("Voici la matrice %d:\n", c);
            afficherMat(tab_m[c-1]);
            break;
        case 3: //donner la valeur d'un �l�ment d'une matrice
            printf("\nPour quelle matrice souhaitez vous obtenir une valeur ? (entrez que le numero)\n");
            printf("Voici les matrices enregistrees :\t");
            while(k<nb_m)
            {
                printf("m%d\t",k+1);
                k++;
            }
            k=0;
            printf("\nVotre choix : ");
            scanf("%d",&c);
            printf("Quelle ligne ? ");
            scanf("%d",&ligne);
            printf("Quelle colonne ? ");
            scanf("%d",&col);
            if(ligne>tab_m[c-1].Nlignes || col>tab_m[c-1].Ncolonnes || ligne<0 || col<0)
            {
                printf("\nErreur, les valeurs rentr�es sont incoh�rentes avec la matrice choisie\n");
                break;
            }
            val=getValue(tab_m[c-1],ligne-1,col);
            printf("la valeur est %d",val);
            break;
        case 4: //changer la valeur d'un �l�ment
            printf("\nPour quelle matrice souhaitez vous changer une valeur ? (entrez que le numero)\n");
            printf("Voici les matrices enregistrees :\t");
            while(k<nb_m)
            {
                printf("m%d\t",k+1);
                k++;
            }
            k=0;
            printf("\nVotre choix : ");
            scanf("%d",&c);
            printf("Quelle ligne ? ");
            scanf("%d",&ligne);
            printf("Quelle colonne ? ");
            scanf("%d",&col);
            if(ligne>tab_m[c-1].Nlignes || col>tab_m[c-1].Ncolonnes || ligne<0 || col<0)
            {
                printf("\nErreur, les valeurs rentr�es sont incoh�rentes avec la matrice choisie\n");
                break;
            }
            printf("Saisir la nouvelle valeur: ");
            scanf("%d",&v);
            if(v == getValue(tab_m[c-1],ligne-1,col)) //on v�rifie que la valeur n'est pas la m�me
            {
                printf("Erreur!La valeur n'a pas change!");
                break;
            }
            putValue(tab_m[c-1],ligne-1,col,v);
            printf("\nLa nouvelle est matrice :\n");
            afficherMat(tab_m[c-1]); //pour des
            break;
        case 5: //addition de deux matrices
            printf("Voici les matrices enregistrees :\t");
            while(k<nb_m)
            {
                printf("m%d\t",k+1);
                k++;
            }
            k=0;
            printf("\nSaisir le numero de matrice m1 (entrez que le numero):\n");
            scanf("%d",&i);
            printf("Saisir le numero de matrice m2 (entrez que le numero):\n");
            scanf("%d",&j);
            addMat(tab_m[i-1],tab_m[j-1]);
            printf("La nouvelle matrice (stocke dans matrice %d) est :\n", i);
            afficherMat(tab_m[i-1]);
            break;
        case 6: //calcul du gain
           printf("\nPour quelle matrice souhaitez vous connaitre le gain d'escpace en octet ? (entrez que le numero)\n");
            printf("Voici les matrices enregistrees :\t");
            while(k<nb_m)
            {
                printf("m%d\t",k+1);
                k++;
            }
            k=0;
            printf("\nVotre choix : ");
            scanf("%d",&c);
            if(nombreOctetsGagnes(tab_m[c-1])<=0)
            {
                printf("il ne gagne pas d'octet\n");
            }
            else
            {
                printf("Le stockage utilisant les linked liste a permi d'economiser %d octets !\n",nombreOctetsGagnes(tab_m[c-1]));
            }
            break;
        case 7:
            a=0;
            printf("\Vous avez choisi de quitter le programme !\n");
            printf("Appuyez sur n'importe quelle touche pour quitter le terminal.");

            break;
        default:
            printf("Erreur!Veuillez ressaisir votre choix svp\n");
            break;
        }
        printf("\n\n");
    }
    while(a!=0);
    //D�sallouer la m�moire
    printf("D�sallocation de la m�moire\n");
    for (int i = 0; i < nb_m; i++) {
        deleteMatrice(tab_m[i]);
        printf("\n");
    }
    return 0;
}
