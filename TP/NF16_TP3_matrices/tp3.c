//************************************************************
// NF16 - TP3 : Les listes chainees
// RAYNAUD Yvain & WU Hanlin
// Définitions des fonctions
//************************************************************

#include "tp3.h"

//------------------------------------------------------------------------------------------------------------------------//


//Question 1 : permettre de remplir une matrice creuse de taille n* m

//Complexité : O(1)
static liste_ligne creerNode(int val, int col)
{
    liste_ligne node = malloc(sizeof(struct element));
    node->col = col;
    node->val = val;
    node->next = NULL;
    return node;
}

//permet de créer une ligne avec l'input de l'utilisateur
//Complexité : O(n)
static void creerLigne(liste_ligne *firstLigne, int line, int  NbCol)
{
    liste_ligne it; //définition d'un itérateur
    for (int i = 0 ; i < NbCol; i++) //on boucle sur toutes les colonnes
    {
        int val;
        printf("M[%d][%d]\t", line, i+1);
        printf("rentrez un entier : ");
        scanf(" %d", &val);
        if (val == 0)
            continue;
        liste_ligne node = creerNode(val, i+1);
        if (*firstLigne == NULL) //pour le premier élément
        {
            *firstLigne = node; //met la valeur au premier élément
            it = *firstLigne; //affectation de du pointeur du premier élément au pointeur
        }
        else
        {
            while(it->next != NULL) //on boucle jusqu'au dernier élément
            {
                it = it->next;
            }
            it->next = node; //on lie le prochain
        }
    }
}

//créer une matrice en appellant n fois la fonction creerLigne()
//Complexité : O(n)
void remplirMat(struct matrice_creuse *m, int N, int M)
{
    int i;
    m->Nlignes = N;
    m->Ncolonnes = M;
    m->lignes = malloc(N*sizeof(liste_ligne)); //on alloue l'espace pour les premiers éléments de chaque ligne
    for(i = 0 ; i < N ; i++) //initialisation de chaque premier élément
    {
        m->lignes[i] = NULL;
    }
    for(i = 0; i < N; i++) //remplissage
    {
        creerLigne(&(m->lignes[i]),i+1, M);
    }

}

//------------------------------------------------------------------------------------------------------------------------//


//Question 2 : permet l'affichage d'une matrice passée en parametre
//Complexité : O(n^2) car double for (affichage de n*m élément obligatoirement)
void afficherMat(struct matrice_creuse m)
{
    for (int i = 0; i < m.Nlignes; i++)
    {
        if (m.lignes[i] == NULL)        //si le premier est nulle, afficher une ligne de 0, comme ça pas besoin de refaire les tests à chaque itération
        {
            for (int i = 0; i < m.Ncolonnes; i++)
                printf("0\t");
        }
        else
        {
            liste_ligne it = m.lignes[i]; //affectation de l'itérateur premier node de la ligne
            for (int j = 0; j < m.Ncolonnes; j++) //on boucle pour toutes les colonnes
            {
                if (it != NULL) //si il y a des valeurs
                {
                    if(it->col != j+1) //si la colonne du node ne correspond pas à la colonne à afficher
                    {
                        printf("0\t");
                    }
                    else                //si cela correspond
                    {
                        printf("%d\t", it->val);
                        it = it->next;
                    }
                }
                else
                    printf("0\t"); //si pas de valeurs afficher 0
            }
        }
        printf("\n"); //Retour à la ligne pour afficher la ligne suivant
    }
}

//------------------------------------------------------------------------------------------------------------------------//


//Question3 : renvoie la valeur de l'élément de la ligne i et de la colonne j de la matrice m
//Complexité : O(n)
int getValue(struct matrice_creuse m,int i,int j)
{
    liste_ligne it=m.lignes[i];
    if(it == NULL)
    {
        return 0;
    }
    else
    {
        while(it!=NULL)
        {
            if(it->col==j)
            {
                return it->val;
            }
            else
            {
                it=it->next;
            }
        }
    }
    return 0; //retourne 0 si ça se situe apres le derniere vraie valuer
}


//------------------------------------------------------------------------------------------------------------------------//

//Question 4 :

//trouver le pred d'un node 'it' passsé en parametre
//Complexité : O(n)
static liste_ligne pred(liste_ligne *tab,liste_ligne it)
{
    liste_ligne e;
    if(it == *tab)
    {
        return NULL;
    }
    else
    {
        e=*tab;
        while(e->next!=it)
        {
            e=e->next;
        }
        return e;
    }
}

//ajoute ou remplace la valeur situé a la ligne i et à la colonne j par la valeur val
//Complexité : O(n)
void putValue(struct matrice_creuse m,int i,int j,int val)
{
    liste_ligne e;
    liste_ligne it = m.lignes[i];
    while((it!=NULL) && (it->col!=j)) //on parcourt les nodes de la ligne
    {
        it = it->next;
    }

    if (val==0)                //val = 0, m[i,j]≠0 , c-a-d le suppression d'un node
    {
        if(pred(&(m.lignes[i]),it)!=NULL)      //supprimer un node entre deux autres nodes
        {
            pred(&(m.lignes[i]),it)->next=it->next;
            free(it);
        }
        else                 //supprimer un node en tete de liste
        {
            e=m.lignes[i];
            m.lignes[i]=m.lignes[i]->next;
            free(e);
        }
    }
    else if (val!=0)
    {
        if(getValue(m,i,j)!= 0)     //val≠0,m[i,j] ≠ 0 -- on aurait aussi pu rajouter un parametre booléen en faisant le test dans le main mais on ne l'a pas fait pr conserver le proto
        {
            it->val=val;
        }
        else          //val≠0,m[i,j]=0,c-a-d l'insertion d'un node
        {
            liste_ligne it1=m.lignes[i];
            liste_ligne it2=m.lignes[i]->next;
            if(m.lignes[i]==NULL)    //si la liste est vide
            {
                m.lignes[i]=creerNode(val,j);
                return;
            }
            e=creerNode(val,j);
            while(it1!=NULL)
            {
                if(it1->col>j)                 //j(debut)->node->node->.....
                {
                    e->next=m.lignes[i];
                    m.lignes[i]=e;
                    break;
                }
                if(it1->col<j)    //node->node->j->node.....
                {
                    if (it2 != NULL)
                    {
                        if (it2->col > j)
                        {
                            e->next=it2;
                            it1->next=e;
                            break;
                        }
                    }
                    if (it1->next==NULL)      //node->node->j(fin)
                    {
                        it1->next=e;
                        break;
                    }
                }
                it1=it1->next;
                it2=it2->next;
            }

        }
    }
}

//------------------------------------------------------------------------------------------------------------------------//

//Q5 : permet de réaliser de maniere optimisée la somme de deux matrices
//Complexité : O(n^2)
void addMat(struct matrice_creuse m1,struct matrice_creuse m2)
{
    liste_ligne e;
    liste_ligne fin=NULL;
    for(int i=0; i<m1.Nlignes; i++)
    {
        liste_ligne it1=m1.lignes[i];
        liste_ligne it2=m2.lignes[i];
        while(it1!=NULL || it2!=NULL)       //le boucle ne s'arrete pas si it1 ou it2 est pas vide
        {
            if(it2 == NULL)              //si it2 est null,on n'a pas besion de changer it1
            {
                it1=it1->next;
                continue;
            }
            if(it1 == NULL)        //si it1 est null,on doit ajouter un node dans la liste,c obligatoire de verifier que si la liste est vide ou pas
            {
                e=creerNode(it2->val,it2->col);
                if(fin != NULL)       //pour le verifier,on ajoute un var de type liste_ligne 'fin',qui retourne le dernier node d'une liste,si la liste est vide,fin=null
                {
                    fin->next=e;
                    fin=e;        //pour toutes les situations,on doit avancer fin
                }
                else
                {
                    m1.lignes[i]=e;
                    fin=m1.lignes[i];    //pour toutes les situations,on doit avancer fin
                }
                it1=fin->next;     //dans ce cas,pour continuer l'additon,on doit avancer it1 a fin->next(c-a-d null) et it2
                it2=it2->next;
                continue;
            }
            if(it1->col == it2->col)    //dans ce cas,on peut seulement changer la val de it1 et verifier que si la val==0 apres l'addition
            {
                it1->val=it1->val+it2->val;
                if(it1->val==0)                      //si le nouveau val est 0,on doit supprimer ce node,ex:-1 + 1 = 0
                {
                    if(pred(&(m1.lignes[i]),it1)!=NULL)      //supprimer un node entre deux autres nodes
                    {
                        e=it1->next;
                        pred(&(m1.lignes[i]),it1)->next=it1->next;
                        free(it1);
                        it1=e;   //avancer it1
                    }
                    else                 //supprimer un node en tete de liste
                    {
                        e=m1.lignes[i];
                        m1.lignes[i]=m1.lignes[i]->next;
                        free(e);
                        it1=m1.lignes[i];  //avancer it1
                    }
                }
                else
                {
                    fin=it1;   //avancer fin
                    it1=it1->next;
                }
                it2=it2->next;  //it1 et it2 ont participe a l'addition,donc on doit avancer tous les deux
                continue;
            }
            if(it1->col < it2->col)   //on ne change pas de it1,et puis avance it1 et fin
            {
                fin=it1;
                it1=it1->next;
                continue;
            }
            if(it1->col > it2->col)   //dans ce cas on doit ajoute un nouveau node et avancer it2 et fin
            {
                e=creerNode(it2->val,it2->col);
                if(pred(&(m1.lignes[i]),it1) == NULL)    //si on l'ajout en tete
                {
                    e->next=m1.lignes[i];
                    m1.lignes[i]=e;
                }
                else
                {
                    pred(&(m1.lignes[i]),it1)->next=e;   //si on l'ajout entre 2 nodes
                    e->next=it1;
                }
                fin=it1;
                it2=it2->next;
            }
        }
    }

}

//------------------------------------------------------------------------------------------------------------------------//

//Q6 : retourne le nombre d'octet gagné par l'utilisation d'une matrice creuse.
//Complexité : O(n^2)
int nombreOctetsGagnes(struct matrice_creuse m)
{
    int res=0;
    for(int i=0; i<m.Nlignes; i++)
    {
        liste_ligne it=m.lignes[i];
        while(it!=NULL)
        {
            res+=2*sizeof(int)+sizeof(int*);
            it=it->next;
        }
    }
    int nb=(m.Nlignes*m.Ncolonnes)*sizeof(int); //calcul de la place en matrice carrée
    printf("Stockage en matrice carree : %d\n", nb);
    printf("Stockage en matrice creuse : %d\n", res);

    return nb - res;
}


//------------------------------------------------------------------------------------------------------------------------//

//désallocation de la mémoire
void deleteList(element *elem)
{
    printf(".");
    if (elem == NULL) {
        return;
    }
    deleteList(elem->next);
    free(elem);
}
void deleteMatrice(struct matrice_creuse m)
{
    printf("Suppression Matrice", m);
    for (int i = 0; i < m.Nlignes; i++) {
        deleteList(m.lignes[i]);
    }
}
